package me.richardboehm.essentials;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.Random;

public class Main extends JavaPlugin {

    private int[] tools = new int[]{/*Flint and Steel*/259,
                                    /*Axes*/271, 275, 258, 279, 286,
                                    /*Fishing Rod*/346,
                                    /*Pickaxes*/270, 274, 257, 285, 278,
                                    /*Shovels*/269, 273, 256, 277, 284,
                                    /*Shears*/359,
                                    /*Hoes*/290, 291, 292, 293, 294,
                                    /*Sword*/268, 272, 267, 276, 283,
                                    /*Bow*/261,
                                    /*Helmets*/298,302,306,310,314,
                                    /*Chestplates*/ 299, 303, 307, 311, 315,
                                    /*Leggings*/ 300, 304, 308, 312, 316,
                                    /*Boots*/ 301, 305, 309, 313, 317};
    private FileConfiguration config;
    private int  level;
    private List<String> deathmessage;

    @Override
    public void onEnable(){

        //Get Configuration File
        config = getConfig();

        //Create Default Config
        config.addDefault("Repair.LevelCost", 1);
        config.addDefault("Kill.Deathmessages", "[has been killed in the deathgames!, was not so nice!]");
        config.options().copyDefaults(true);
        saveConfig();

        //Read Values
        level = config.getInt("Repair.LevelCost");
        deathmessage = config.getStringList("Kill.Deathmessages");
        getServer().getConsoleSender().sendMessage(String.valueOf(deathmessage.size()));
    }

    @Override
    public void onDisable(){
        //do something maybe
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){

        if(cmd.getName().equalsIgnoreCase("ccrepair")){
            repair(sender);
            return true;
        }else
        if (cmd.getName().equalsIgnoreCase("cckill")) {
            kill(sender, args);
            return true;
        }
        return false;
    }

    public  boolean kill(CommandSender sender, String[] args){

        //Get the player
        Player player = getServer().getPlayer(args[0]);
        //Checks if he is online
        if(player != null){
            //Rolls some dice
            Random r = new Random();
            int i = r.nextInt(deathmessage.size());
            player.setHealth(0.0D);
            //Broadcast random death message
            getServer().broadcastMessage(ChatColor.RED + args[0]+ " " + deathmessage.get(i));
        }else{
            sender.sendMessage(ChatColor.RED + "Player is not online!");
        }
        return true;
    }

    public boolean repair(CommandSender sender){

        //Is the sender a Player?
        if(sender instanceof Player){
            Player p = (Player) sender;
            org.bukkit.inventory.ItemStack item = p.getItemInHand();
            short durability = item.getDurability();
            //Checks if Item is a tool
            if(durability != -1){
                if(p.getLevel() >= level){
                    //Remove the exp from the player
                    p.setLevel(p.getLevel() - level);
                    item.setDurability((short) 0);
                }else{
                    sender.sendMessage("You need at least " + level + " Level!");
                }
            }else{
                sender.sendMessage("You must hold a tool!");
            }
        }else{
            sender.sendMessage("You must be a player!");
        }
        return true;
    }
}